; Ph nglui mglw nafh Cthulhu R lyeh wgah nagl fhtagn
; Hапоминалка: 
; mov rax, 1   Код системного вызова "Вывод"
; mov rdi, 1   Дескриптор stdout
; syscall      Системный вызов
SYS_WRITE equ 1
SYS_EXIT  equ 60
STDOUT    equ 1
DECA_NUMSYS_BASE equ 10
section .text
 
; Принимает код возврата и завершает текущий процесс
; из семинара
exit: 
    mov rax, SYS_EXIT
    xor rdi, rdi,
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; из семинара
string_length:
    xor rax,rax ; счетчик не тот кто считает, а кого считают
    .loop: 
        cmp byte[rax+rdi],0
        je .end
        inc rax 
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov rdx, rax ;Длина строки
    mov rsi, rdi ;Адрес строки
    mov rdi, STDOUT 
    mov rax, SYS_WRITE
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi ; символ в память
    mov rdx, 1     
    mov rsi, rsp ; ссылку на адрес символа в rsi потому что системный вызов будет брать адрес из него
    pop rdi
    mov rax, SYS_WRITE      ; Код системного вызова "Вывод"
    mov rdi, STDOUT ; Дескриптор stdout
    syscall         ; Системный вызов
    ret

; Переводит строку (выводит символ с кодом 0xA)
;из семинара
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax 
    mov r8, rsp 
    mov r9, DECA_NUMSYS_BASE; 10-СС 
    mov rax, rdi
    push 0 ; символ конца строки в  конце(т.к проход от для записис в стек от конца к началу)
    .loop:
        xor rdx,rdx
        div r9 ;целая часть => rax, остаток => rdx
        add rdx, '0' ; => ASCII
        dec rsp 
        mov byte[rsp], dl ;символ из rdx записывается по адресу rsp
        cmp rax,0
        je .end
        jmp .loop

    .end:
        mov rdi, rsp
        push r8
        call print_string
        pop r8
        mov rsp, r8
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    print_int:
    xor rax, rax
    cmp rdi,0
    jl .negative ; если чсло<0 
    jmp print_uint ; если число>=0

    .negative
        push rdi
        mov rdi, '-'    ; вывод '-'
        call print_char 
        pop rdi
        neg rdi ;инверсия
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
    .loop:
        mov dl, byte[rdi + rcx]
        cmp byte[rsi + rcx], dl
        jne .end
        inc rcx
        cmp dl, 0
        jne .loop
        inc rax
    .end:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi,rdi 
    mov rdx, 1; Длина
    push 0 ; 
    mov rsi, rsp ; адрес символа
    syscall
    pop rax ; сивмол загружается со стека в rax
    ret 
 
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rcx, rcx
    xor rax, rax
    .loop:
        push rcx
        push rsi
        push rdi
        call read_char ; символ в rax
        pop rdi
        pop rsi
        pop rcx

        ; проверка конца
        cmp rax, 0 
        je .end

        ; проверка на соответствие 0x20, 0x9, 0xA
        cmp rax, ' '
        je .skip_symbols
        cmp rax, '	'
        je .skip_symbols
        cmp rax, '\n'
        je .skip_symbols

        mov [rdi+rcx], rax
        inc rcx ;i++
        cmp rcx, rsi ; при переполнении буфера переход к .err
        jge .err
        jmp .loop

    .skip_symbols:
        cmp rcx,0 ; если слово еще не началась, то пропуск.
        je .loop
        jmp .end
    .err:                                                                
        xor rax, rax ; возврат 0                                         
        xor rdx, rdx
        ret
    .end:
        xor rax, rax
        mov [rdi+rcx], rax 
        mov rdx, rcx ; длина
        mov rax, rdi ; адрес
        ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, DECA_NUMSYS_BASE ; 10-СС
    .loop:
        movzx r9, byte[rdi+rcx] 
        cmp r9,0
        je .end
        cmp r9b, '0' ; проверка на число...числовитость...численность?
        jl .end
        cmp r9b, '9'
        jg .end

        mul r8 
        sub r9b, '0'
        add rax, r9 
        inc rcx ;i++
        jmp .loop

    .end
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    mov rcx, rdi 
    cmp byte[rcx], '-'
    je .negative
    jmp .positive
    .negative:
        inc rcx ;
        mov rdi, rcx
        push rcx
        call parse_uint
        pop rcx
        neg rax 
        inc rdx
        ret
    .positive:
        mov rdi, rcx
        jmp parse_uint

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    push rsi
    push rdi
    push rcx
    push rdx
    call string_length
    pop rdx
    pop rcx
    pop rdi
    pop rsi

    mov r8, rax
    cmp rdx, r8
    jl .error
    .loop:
        cmp rcx, r8
        jg .end
        mov r10,[rdi+rcx] 
        mov [rsi+rcx], r10
        inc rcx; i++
        jmp .loop
    
    .error:
        mov rax, 0
        ret
    .end:
        mov rax, r8
        ret
